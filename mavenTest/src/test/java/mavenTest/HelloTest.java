package mavenTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.tut.Hello;

public class HelloTest {

	@Test(expected = IllegalArgumentException.class)
	public void testNull() {
		Hello.helloName(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmpty() {
		Hello.helloName("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyTab() {
		Hello.helloName("	");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptySpace() {
		Hello.helloName(" ");
	}

	@Test
	public void testNameWithDigits() {
		assertEquals("Hello 01Name!", Hello.helloName("01Name"));
	}

	@Test
	public void testLowerCaseName() {
		assertEquals("Hello Gabi!", Hello.helloName("gabi"));
	}

	@Test
	public void testUpperCaseName() {
		assertEquals("Hello Gabi!", Hello.helloName("Gabi"));
	}
}
