package mavenTest;

import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tut.HelloWorldSwing;

public class HelloWorldSwingTest {

	private FrameFixture demo;

	@Before
	public void setUp() {
		demo = new FrameFixture(new HelloWorldSwing());
	}

	@After
	public void tearDown() {
		demo.cleanUp();
	}

	@Test
	public void test() {
		((HelloWorldSwing) demo.target).show("gabika");
		demo.label("helloLabel").requireText("Hello Gabika!");
	}

	@Test
	public void test2() {
		((HelloWorldSwing) demo.target).show("gabika");
		demo.label("helloLabel").requireText("Hello Gabika!");
	}
}
