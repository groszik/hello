package com.tut;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloWorldSwing extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -5245866502569707996L;
	private final JLabel label;

	public HelloWorldSwing() {
		super("HelloWorldSwing");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		label = new JLabel(" ");
		label.setName("helloLabel");
		getContentPane().add(label);
		pack();
	}

	public void show(final String name) {
		label.setText(Hello.helloName(name));
		setVisible(true);
	}

	public static void main(final String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new HelloWorldSwing().show("gabo");
			}
		});
	}
}