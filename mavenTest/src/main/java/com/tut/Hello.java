package com.tut;

public class Hello {

	private static void checkHelloArguments(final String name) {
		if (name == null || "".equals(name.trim()))
			throw new IllegalArgumentException();
	}

	private static String capitalizeFirstLetter(final String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	private static String getGreeting(final String name) {
		return new StringBuilder().append("Hello ")
				.append(capitalizeFirstLetter(name)).append("!").toString();
	}

	public static String helloName(final String name) {
		checkHelloArguments(name);
		return getGreeting(name);
	}

	public static void checkArgumentCount(final String[] args) {
		if (args.length != 1)
			throw new IllegalArgumentException();
	}

	public static void main(final String[] args) {
		checkArgumentCount(args);
		System.out.println(helloName(args[0]));
	}

}
